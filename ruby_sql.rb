# Author:  Agrim Pathak
# Version: v0.1
# Date:    2017-06

require 'csv'
require 'set'

module RubySQL

  # # # # # # # # # # # #
  #      Expressions    #
  # # # # # # # # # # # #

  def SELECT(*fields)
    validate_query_fields(fields)
    QueryBuilder.new(fields.to_set)
  end

  def SUM(*fields)
    msg = "Bad args passed to SUM"
    raise msg unless fields.all? { |f| f.is_a?(String) }
    SumAggregator.new(fields)
  end

  def DIFF(*fields)
    msg = "Bad args passed to DIFF"
    raise msg unless fields.all? { |f| f.is_a?(String) }
    DiffAggregator.new(fields)
  end

  def PRODUCT(*fields)
    msg = "Bad args passed to PRODUCT"
    raise msg unless fields.all? { |f| f.is_a?(String) }
    ProductAggregator.new(fields)
  end

  def DIVIDE(*fields)
    msg = "Bad args passed to DIVIDE"
    raise msg unless fields.all? { |f| f.is_a?(String) }
    DivideAggregator.new(fields)
  end

  def SUMPRODUCT(*fields)
    msg = "Bad args passed to PRODUCT"
    raise msg unless fields.all? { |f| f.is_a?(String) }
    SumProductAggregator.new(fields)
  end

  def MIN(field)
    msg = "Bad args passed to MIN"
    raise msg unless field.is_a?(String)
    MinAggregator.new(field)
  end

  def MAX(field)
    msg = "Bad args passed to MAX"
    raise msg unless field.is_a?(String)
    MaxAggregator.new(field)
  end

  private
  def validate_query_fields(fields)
    msg = "Bad args passed to SELECT"
    raise msg unless fields.to_a.flatten.all? do
      |f| f.is_a?(String) || f.is_a?(Aggregator)
    end
  end


  # # # # # # # # # # # #
  #       Objects       #
  # # # # # # # # # # # #

  #TODO: Use existing index in Table instead of creating a new
  # one every time it's needed.  Currently creates new index
  # every time to keep code bug-proof

  class Table

    attr_reader :records
    attr_reader :index

    def initialize(records = [])
      @records = records
      @index   = {}
    end

    def CREATE_INDEX(field)
      @index[field].clear if !@index[field].nil?
      @index[field] =
        @records.each_with_index.inject({}) do |h, rec_idx|
          rec = rec_idx.first
          idx = rec_idx.last
          field_indices = h[rec[field]] || []
          field_indices.push(idx)
          h[rec[field]] = field_indices; h
        end
    end

    def INSERT(record)
      @index.each do |fld,val|
        rv = @records[fld]
        i = val[rv] || []
        i.push(@records.size)
        val[rv] = i
      end
      @records.push(record)
      self
    end

    def UNION(tbl)
      @records.push(*tbl.records)
      ind_keys = @index.keys
      @index = {}
      ind_keys.each do |i|
        CREATE_INDEX(i)
      end
      self
    end

    def ROUND(field, n)
      @records.each { |r| r[field] = r[field].round(n) };
      self
    end

    def ADD_COLUMN(tbl)
      @records.zip(tbl.records).each do |p|
        p.first.merge!(p.last)
      end
      self
    end

    def ADD_STATIC_COLUMN(fld, val)
      @records.each { |r| r[fld] = val }
      self
    end

    def COPY_TABLE
     #TODO copy indices
     Table.new(@records.inject([]){ |rec, r| rec.push(r.clone) })
    end

    def self.IMPORT_CSV(path, schema, has_headers)
      recs = CSV.foreach(path).inject([]) do |rec, row|
        rec.push(
          schema.zip(row.compact).each_with_object({}) do |f, r|
            name = f[0][0]
            type = f[0][1]
            value = convert(f[1], type)
            r[name] = value
          end
          )
      end
      (recs[0] = recs[-1]; recs.pop) if has_headers # hacky?
      Table.new(recs)
    end

    def EXPORT_CSV(path, with_header=true, header=nil)
      return File.open(path, "w") {} if @records.empty?
      header ||= @records[0].keys
      File.open(path, "w") do |csv|
        header.each { |h| csv << h << "," } && csv << "\n" if with_header
        @records.each do |r|
          header.each { |h| csv << r[h].to_s << "," }
          csv << "\n"
        end
      end
    end

    def WITH_ROW_NUMBER(fld_nme="row_number")
      @records.each_with_index { |r, i| r.merge!({ fld_nme => i }) }
      self
    end

    def ROW_COUNT
      @records.size
    end

    def EMPTY?
      @records.empty?
    end

    # semi-private methods
    def select_fields(fields)
      return records if fields.include?("*")
      fields += fields.map do |f|
        f.field if f.is_a?(Aggregator)
      end
      .flatten.to_set
      @records.each do |r|
        r.keep_if { |f| fields.include?(f) }
      end
    end

    def sort!(fields)
      return self unless fields
      @records.sort_by! { |r| fields.map{ |f| r[f] } }
      self
    end

    def fetch_indices(indices)
      indices.map { |i| @records[i] }
    end

    private
    def self.convert(v,t)
      t == :int ? v.to_i :
      t == :flt ? v.to_f :
      t == :bin ? (v.to_i > 0 ? 1 : 0) :
      v
    end

  end

  # # # # # # # # # # # #
  #       Builders      #
  # # # # # # # # # # # #

  class QueryBuilder

    def initialize(query_fields, table=nil)
      @query_fields = query_fields
      @table = table
      @group_by = [].to_set
    end

    def SELECT(*fields)
      validate_query_fields(fields)
      @query_fields = fields.to_set
      self
    end

    def FROM(tbl)
      msg = "Unexpected 'FROM'"
      raise msg unless @table.nil?
      @table = tbl.COPY_TABLE
      self
    end

    def INNER_JOIN(tbl)
      InnerJoinBuilder.new(@table, tbl, @query_fields)
    end

    def WHERE(where_field)
      WhereBuilder.new(@table, @query_fields, where_field)
    end

    def AND(where_field) # where
      WhereBuilder.new(@table, @query_fields, where_field)
    end

    def AND(key, key2) # joins
      WhereBuilder.new(@table, @query_fields, key).EQUALS(key2)
    end

    def GROUP_BY(*groups)
      msg = "Unexpected 'GROUP_BY'"
      raise msg unless @group_by.empty?
      @group_by = groups.to_set
      @query_fields += @group_by
      self
    end

    def ORDER_BY(*field)
      @order_by = field
      self
    end

    # Pipe current query into another one (syntactic sugar for inner query)
    def >>
      QueryBuilder.new(nil, execute)
    end

    def execute
      agg = @query_fields.select{ |q| q.is_a?(Aggregator) }.to_set
      non_agg = @query_fields - agg
      msg = "Non-aggegator fields exist not contained in GROUP_BY"
      raise msg unless agg.empty? || non_agg == @group_by
      @table.select_fields(@query_fields + @group_by)
      return @table.sort!(@order_by) if agg.empty?
      return Table.new(
        agg.inject({}) { |h, a| h.merge({a.name => a.apply(@table.records)}) }
        ).sort!(@order_by) if @group_by.empty?
      @group_by.each { |g| @table.CREATE_INDEX(g) }
      Table.new(
        @group_by.inject([nil]) { |prd, grp| prd.product(fetch_distinct(grp)) }
        .flatten(-1)
        .compact
        .each_slice(@group_by.size).inject([]) { |a, s| a.push(s) }
        .inject([]) do |rec, prd|
          rows = prd.inject((0..@table.ROW_COUNT).to_a) do |row, prd|
            k = prd.keys.first
            v = prd[k]
            row & @table.index[k][v]
          end
          next rec if rows.empty?
          rec.push(
            prd.inject({}) { |r, p| r.merge(p) } # GROUP_BY fields
            .merge(
              agg.inject({}) { |r, a|            # Aggregator fields
                r.merge({a.name => a.apply(@table.fetch_indices(rows))})
              }
              )
            )
        end
        ).sort!(@order_by)
    end

    private
    def fetch_distinct(field)
      @table.CREATE_INDEX(field)
      .keys
      .each
      .inject([]) { |r, k| r.push({field => k}) }
    end

  end

  class WhereBuilder

    def initialize(tbl, query_fields, where_field)
      @table          = tbl
      @query_fields = query_fields
      @where_field  = where_field
      @table.index.clear
    end

    def EQUALS(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] == r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] == arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def DOES_NOT_EQUALS(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] != r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] != arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_GREATER_THAN(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] > r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] > arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_LESS_THAN(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] < r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] < arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_ATLEAST(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] >= r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] >= arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def DOES_NOT_EXCEED(arg)
      arg.is_a?(String) ?
        @table.records.keep_if{ |r| r[@where_field] <= r[arg] } :
        @table.records.keep_if{ |r| r[@where_field] <= arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_BETWEEN(lo,hi)
      @table.records.keep_if{ |r| r[@where_field].between?(lo,hi) }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_NOT_BETWEEN(lo,hi)
      @table.records.keep_if{ |r| !r[@where_field].between?(lo,hi) }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_STRICTLY_BETWEEN(lo,hi)
      @table.records.keep_if{ |r| lo < r[@where_field] && r[@where_field] < hi }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_POSITIVE
      @table.records.keep_if{ |r| r[@where_field] > 0 }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_NEGATIVE
      @table.records.keep_if{ |r| r[@where_field] < 0 }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_NON_NEGATIVE
      @table.records.keep_if{ |r| r[@where_field] >= 0 }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS_NON_POSITIVE
      @table.records.keep_if{ |r| r[@where_field] <= 0 }
      QueryBuilder.new(@query_fields, @table)
    end

    def IS(arg)
      @table.records.keep_if{ |r| r[@where_field] == arg }
      QueryBuilder.new(@query_fields, @table)
    end

    def IN(*args)
      arg_set = args.to_set
      @table.records.keep_if{ |r| arg_set.include?(r[@where_field]) }
      QueryBuilder.new(@query_fields, @table)
    end

  end


  class InnerJoinBuilder

    def initialize(tbl1, tbl2, query_fields)
      @t1 = tbl1
      @t2 = tbl2
      @query_fields = query_fields
    end

    def ON(key1, key2)
      return QueryBuilder.new(@query_fields, Table.new) if
        @t1.EMPTY? || @t2.EMPTY?
      key1,key2 = key2, key1 if
        @t1.records.first.has_key?(key2) &&
        @t2.records.first.has_key?(key1)
      ind1 = @t1.CREATE_INDEX(key1)
      ind2 = @t2.CREATE_INDEX(key2)
      QueryBuilder.new(
        @query_fields,
        Table.new(
            (ind1.keys & ind2.keys).inject([]) do |rec, key|
              rec.push(
                @t1.fetch_indices(ind1[key])
                     .product(@t2.fetch_indices(ind2[key]))
                     .map { |a| a.first.merge(a.last) }
                )
            end
            .flatten
          )
        )
    end

  end

  class Aggregator
    attr_reader :field
    attr_reader :name
    def initialize(field)
      @field = field
      @name = "untitled"
    end

    def AS(name)
      @name = name
      self
    end
  end

  class SumAggregator < Aggregator
    def apply(records)
      records.inject(0.0) do |s, r|
        @field.each do |f|
          s += r[f]
        end
        s
      end
    end
  end

  class DiffAggregator < Aggregator
    def apply(records)
      records.inject(0.0) do |d, r|
        d = r[@field[0]]
        @field[1..-1].each do |f|
          d -= r[f]
        end
        d
      end
    end
  end

  class ProductAggregator < Aggregator
    def apply(records)
      records.inject(1.0) do |p, r|
        @field.each do |f|
          p *= r[f]
        end
        p
      end
    end
  end

  class DivideAggregator < Aggregator
    def apply(records)
      records.inject(0.0) do |d, r|
        d = r[@field[0]]
        @field[1..-1].each do |f|
          d /= r[f]
        end
        d
      end
    end
  end

  class SumProductAggregator < Aggregator
    def apply(records)
      records.inject(0.0) do |sp, r|
        sp += @field.inject(1.0) { |p, f| p *= r[f] }
      end
    end
  end

  class MinAggregator < Aggregator
    def apply(records)
      records.map { |r| r[@field] }.min
    end
  end

  class MaxAggregator < Aggregator
    def apply(records)
      records.map { |r| r[@field] }.max
    end
  end

end
